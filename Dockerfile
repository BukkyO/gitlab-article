FROM node:10

# Create app directory
RUN mkdir app
WORKDIR /app

# Install app dependencies
COPY package*.json ./
RUN npm install

# Bundle app source
COPY . /app

# Expose your app's default port
EXPOSE 3000

CMD [ "npm", "start" ]
