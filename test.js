const chai = require("chai");
const chaiHttp = require("chai-http");
const { expect } = require("chai");
const app = require('./index');

chai.use(chaiHttp);

describe('Testing Entry Suite', () => {
    it('Test the entry point', (done) => {
        chai.request(app)
            .get('/')
            .set('Accept', 'application/json')
            .end((err, res) => {
                expect(res.status).to.have.equal(200);
                expect(res.body.message).to.be.equal('Hello World of CI/CD');
                done();
            });
    });
});
